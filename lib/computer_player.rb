#Both HumanPlayer and ComputerPlayer should have the same API;
# they should have the same set of public methods.
# This means they should be interchangeable.

class ComputerPlayer

  def initialize(name)
    @name = name
    @mark = :O  #human player always X so computer must always be O
  end


  attr_accessor :mark, :name, :board  #just reader?


  #stores board that is passed to it as an instance variable so that get_move has access to it
  def display(board)
    @board = board
  end

  #return a winning move if one is available, otherwise move randomly
  def get_move

    #populate an array with all positions currently == nil
    potential_moves = Array.new
    (0..2).each do |row|
      (0..2).each do |col|
        potential_moves << [row, col] if @board.grid[row][col].nil?
      end
    end

    #copy the board so I don't mess with actual board; place_mark should be used to mark the board AFTER get_move decides the position
    copy_of_board = @board.grid.dup

    #click through array of potential moves
    potential_moves.each do |position|
      copy_of_board[position[0]][position[1]] = :O #add the :O mark in that position
      return position if board.winner(copy_of_board) #== true; return the position if it results in win
      copy_of_board[position[0]][position[1]] = nil #set this position back to nil for next iteration
    end

    potential_moves.sample #return random potential move if we exit the loop b/c no winning moves available
  end

end#of class
