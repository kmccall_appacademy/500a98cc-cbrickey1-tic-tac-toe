class Board

  def initialize(grid=3)
    @grid = grid == 3 ? Array.new(3) { Array.new(3) } : grid
  end

  attr_reader :grid  #just reader?

  #position is an array [outer_index, inner_index] like my_array[0][0] but written [0, 0]
  def [](position)
    row, col = position
    @grid[row][col]
  end

  #assigns mark (:X or :O) to a position in the grid
  def[]=(position, mark)
    row, col = position
    @grid[row][col] = mark
  end

  #maybe use method []= instead
  def place_mark(position, mark)
    #throw error if position is not empty
    row, col = position
    @grid[row][col] = mark

  end

  def empty?(position)
    row, col = position
    @grid[row][col].nil? ? true : false

    # if @grid[position].nil? ? true : false
  end

  #returns a mark if winner; returns nil if no winner
  #attempted to place returns into a result variable so only return once at end, didn't work
  def winner(grid=@grid)

    #ROWS
    x_wins_row = (0..2).any? do |row|
      grid[row][0] == :X && grid[row][1] == :X && grid[row][2] == :X
    end

    o_wins_row = (0..2).any? do |row|
      grid[row][0] == :O && grid[row][1] == :O && grid[row][2] == :O
    end

    #COLUMNS
    x_wins_col = (0..2).any? do |col|
      grid[0][col] == :X && grid[1][col] == :X && grid[2][col] == :X
    end

    o_wins_col = (0..2).any? do |col|
      grid[0][col] == :O && grid[1][col] == :O && grid[2][col] == :O
    end

    if x_wins_row == true
      return :X
    elsif o_wins_row == true
       return :O
    elsif x_wins_col == true
      return :X
    elsif o_wins_col == true
      return :O
    end

    #DIAGONALS
    if grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
      return :X
    elsif grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
      return :X
    elsif grid[0][0] == :O && grid[1][1] == :O && grid[2][2] == :O
      return :O
    elsif grid[0][2] == :O && grid[1][1] == :O && grid[2][0] == :O
      return :O
    end

    nil
  end


  def over?(grid=@grid)

    catseye = (0..2).none? do |row|
      grid[row][0].nil? && grid[row][1].nil? && grid[row][2].nil?
    end

    result =
      if winner(grid).nil? == false
        true
      elsif catseye == true
        true
      else
        false
      end

    result
  end#of method


end#of class




# Example using [] method:
# position = [0, 0]
# board[position]

# Example using []= method:
# position = [0, 3]
# board[position] = :X
#

#works only with instance variable grid
# def winner
#
#   #ROWS
#   x_wins_row = (0..2).any? do |row|
#     @grid[row][0] == :X && @grid[row][1] == :X && @grid[row][2] == :X
#   end
#
#   o_wins_row = (0..2).any? do |row|
#     @grid[row][0] == :O && @grid[row][1] == :O && @grid[row][2] == :O
#   end
#
#   #COLUMNS
#   x_wins_col = (0..2).any? do |col|
#     @grid[0][col] == :X && @grid[1][col] == :X && @grid[2][col] == :X
#   end
#
#   o_wins_col = (0..2).any? do |col|
#     @grid[0][col] == :O && @grid[1][col] == :O && @grid[2][col] == :O
#   end
#
#   if x_wins_row == true
#     return :X
#   elsif o_wins_row == true
#      return :O
#   elsif x_wins_col == true
#     return :X
#   elsif o_wins_col == true
#     return :O
#   end
#
#   #DIAGONALS
#   if @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X
#     return :X
#   elsif @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X
#     return :X
#   elsif @grid[0][0] == :O && @grid[1][1] == :O && @grid[2][2] == :O
#     return :O
#   elsif @grid[0][2] == :O && @grid[1][1] == :O && @grid[2][0] == :O
#     return :O
#   end
#
#   nil
# end
