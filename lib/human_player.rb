#Both HumanPlayer and ComputerPlayer should have the same API;
# they should have the same set of public methods.
# This means they should be interchangeable.

class HumanPlayer

def initialize(name)
  @name = name
  @mark = :X  #human player always X so computer must always be O
end

attr_accessor :mark, :name, :display #just reader?


#print board to console
def display(board)
  print board.grid
end


#translates human instruction
def get_move
  puts "Where would you like to move? Type in the form of row, column."
  position_string = $stdin.string
  [position_string[0].to_i, position_string[-1].to_i]
end


end#of class
