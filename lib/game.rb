require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'


# Your Game class should be passed two player objects on instantiation;
# because both player classes have the same API,
# the game should not know nor care what kind of players it is given.
class Game

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = @player_one
    @board = Board.new
  end


  attr_accessor :player_one, :player_two, :board, :current_player


  def switch_players!
    @current_player = @current_player == @player_one ? @player_two : @player_one
  end


  #handles the logic for a single turn
  def play_turn
    move = current_player.get_move  #issue is with this line, it's not picking up the test's player
    @board.place_mark(move, current_player.mark)
    self.switch_players!
  end


  #calls play_turn each time through aloop until game is over (using board's over? method???)
  def play
    # while @board.over? == false
    #   @play_turn
    # end
  end

end#of class
